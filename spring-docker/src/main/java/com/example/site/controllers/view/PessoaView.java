package com.example.site.controllers.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PessoaView {

	private Long id;
	private String nome;
}
