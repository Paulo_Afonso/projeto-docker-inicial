import { TestBed } from '@angular/core/testing';

import { CursoAngularService } from './curso-angular.service';

describe('CursoAngularService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CursoAngularService = TestBed.get(CursoAngularService);
    expect(service).toBeTruthy();
  });
});
