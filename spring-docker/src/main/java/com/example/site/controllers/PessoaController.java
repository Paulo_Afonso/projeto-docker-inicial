package com.example.site.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.site.controllers.view.PessoaView;
import com.example.site.services.PessoaService;

@RestController
@RequestMapping("/service/pessoas")
public class PessoaController {

	@Autowired
	PessoaService pessoaService;
	
	@GetMapping(value="")
	@ResponseBody
	public ResponseEntity<List<PessoaView>> findPessoa() {
		return pessoaService.findPessoa();
	}
}
