package com.example.site.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.site.controllers.view.PessoaView;
import com.example.site.db.models.Pessoa;
import com.example.site.db.reposiories.PessoaRepository;

@Service
public class PessoaService {
	
	@Autowired
	PessoaRepository pessoaRepository;

	public ResponseEntity<List<PessoaView>> findPessoa() {
		List<Pessoa> pessoaList = pessoaRepository.findAll();
		List<PessoaView> view = new ArrayList<>();
		for (Pessoa pessoa : pessoaList) {
			PessoaView pessoaV = new PessoaView();
			pessoaV.setId(pessoa.getId());
			pessoaV.setNome(pessoa.getNome());
			view.add(pessoaV);
		}
		return new ResponseEntity<List<PessoaView>>(view, HttpStatus.OK);
	}
}
