package com.example.site.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.site.controllers.view.CursoView;
import com.example.site.db.models.Curso;
import com.example.site.db.reposiories.CursoRepository;

@Service
public class CursoServicce {
	
	@Autowired
	private CursoRepository cursoRepository;

	public ResponseEntity<List<CursoView>> findAll() {
		List<CursoView> viewResp = new ArrayList<>();
		List<Curso> listCurso = cursoRepository.findAll();
		
		for (Curso curso : listCurso) {
			CursoView view = new CursoView();
			view.setId(curso.getId());
			view.setNome(curso.getNome());
			viewResp.add(view);
		}
		
		return new ResponseEntity<>(viewResp, HttpStatus.OK);
	}
}
