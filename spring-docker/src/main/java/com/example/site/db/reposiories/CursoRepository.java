package com.example.site.db.reposiories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.site.db.models.Curso;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long>{

}
