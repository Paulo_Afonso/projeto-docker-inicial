package com.example.site.controllers.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CursoView {

	private Long id;
	private String nome;
}
