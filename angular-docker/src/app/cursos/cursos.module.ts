import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CursoAngularComponent } from './curso-angular/curso-angular.component';
import { MatInputModule, MatButtonModule, MatSelectModule, MatIconModule } from '@angular/material';



@NgModule({
  declarations: [ CursoAngularComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule
  ],
  exports: [
    CursoAngularComponent
  ]
})
export class CursosModule { }
